package dev.alexroberts.podcastdownloader;

public final class Constants {

    public static final String WORKING_DIR = "./.data";
    public static final String TEMP_DIR = WORKING_DIR + "/tmp";
    public static final String IMAGE_DIR = WORKING_DIR + "/images";
    public static final String DATABASE_DIR = WORKING_DIR + "/database";

    public static final String IMAGE_RESOURCE_PATTERN = "/images/**";
    public static final String IMAGE_RESOURCE_LOCATIONS = "file:" + IMAGE_DIR + "/";

    /**
     * Hidden constructor
     */
    private Constants() {
        // do nothing.
    }
}
