package dev.alexroberts.podcastdownloader;

import org.springframework.context.annotation.Configuration;
import org.springframework.lang.NonNull;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import static dev.alexroberts.podcastdownloader.Constants.IMAGE_RESOURCE_LOCATIONS;
import static dev.alexroberts.podcastdownloader.Constants.IMAGE_RESOURCE_PATTERN;

@Configuration
@EnableWebMvc
public class MvcConfig implements WebMvcConfigurer {

    private static final String[] RESOURCE_LOCATIONS = {
        "classpath:/META-INF/resources/",
        "classpath:/resources/",
        "classpath:/static/",
        "classpath:/public/"
    };

    @Override
    public void addResourceHandlers(@NonNull ResourceHandlerRegistry registry) {
        if (!registry.hasMappingForPattern("/**")) {
            registry.addResourceHandler("/**").addResourceLocations(RESOURCE_LOCATIONS);
        }

        registry.addResourceHandler(IMAGE_RESOURCE_PATTERN)
            .addResourceLocations(IMAGE_RESOURCE_LOCATIONS);
    }
}
