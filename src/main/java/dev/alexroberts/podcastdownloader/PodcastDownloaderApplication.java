package dev.alexroberts.podcastdownloader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PodcastDownloaderApplication {

    public static void main(String[] args) {
        SpringApplication.run(PodcastDownloaderApplication.class, args);
    }
}
