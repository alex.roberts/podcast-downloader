package dev.alexroberts.podcastdownloader.service.json;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import dev.alexroberts.podcastdownloader.util.FileUtils;

public final class JSONFileUtil {
    private static final Logger LOG = LoggerFactory.getLogger(JSONFileUtil.class);

    public static <T> Optional<List<T>> readJSONArrayFromFile(String uri, Class<T> objectClass) {
        final ObjectMapper mapper = new ObjectMapper();

        if (!FileUtils.isValidFile(uri)) {
            LOG.warn("Attempted to read non-existant file {}. File will be created.", uri);
            createFile(uri, objectClass);
            return Optional.empty();
        }

        try (FileReader reader = new FileReader(uri)) {
            JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, objectClass);
            return Optional.of(mapper.readValue(reader, type));
        } catch (IOException e) {
            LOG.error("Failed to read JSON file.", e);
        }

        return Optional.empty();
    }

    public static <T> void writeFile(String uri, T jsonContent) {
        if (!FileUtils.isValidFile(uri)) {
            LOG.warn("Attempted to write non-existant file {}. File will be created.", uri);
            createFile(uri, jsonContent.getClass());
        }

        File f = new File(uri);
        try {
            final ObjectMapper mapper = new ObjectMapper();
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
            mapper.writeValue(f, jsonContent);
        } catch (Exception e) {
            LOG.error("Failed to write file", e);
        }
    }

    private static <T> void createFile(String uri, Class<T> contentClass) {
        LOG.debug("Creating file at '{}'", uri);

        // Maybe create the directory structure, if needed.
        int lastSlash = uri.lastIndexOf("/");
        if (lastSlash > -1) {
            String directory = uri.substring(0, lastSlash);
            File dir = new File(directory);
            dir.mkdirs();
        }

        File f = new File(uri);
        try {
            boolean success = f.createNewFile();
            if (success) {
                final ObjectMapper mapper = new ObjectMapper();
                mapper.writeValue(f, contentClass.getConstructor().newInstance());
            }
        } catch (Exception e) {
            LOG.error(String.format("Failed to create file at '%s'", uri), e);
        }
    }
}
