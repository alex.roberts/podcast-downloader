package dev.alexroberts.podcastdownloader.service;

import dev.alexroberts.podcastdownloader.model.EpisodeDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * Various utilities for RSS data.
 */
public final class RSSUtils {
    private static final Logger LOG = LoggerFactory.getLogger(RSSUtils.class);

    public static final String EPISODE_DATE_PATTERN = "EEE, d MMM yyyy HH:mm:ss Z";
    public static final String EPISODE_DATE_OUTPUT = "MMM dd, yyyy";

    private static final String AD_CHOICES = "Learn more about your ad choices. Visit podcastchoices.com/adchoices";

    private static final String UNDERSCORE = "_";
    private static final String ILLEGAL_FOLDER_NAME_REGEX = "[^a-zA-Z\\d.-]";
    private static final String ILLEGAL_OS_CHARACTER_REGEX = "[\\\\/:*?\"<>|]";

    private static final int MP3_IMAGE_SIZE = 300;

    private static final String EPISODE_DURATION_REGEX = "(\\d{2}:\\d{2}:\\d{2})";

    /**
     * Remove unnecessary information from the given podcast episode description.
     *
     * @param podcastEpisodeDescription Description of a podcast episode.
     * @return The provided episode description with components removed.
     */
    public static String cleanUpDescription(String podcastEpisodeDescription) {
        return podcastEpisodeDescription.replace(AD_CHOICES, "").trim();
    }

    /**
     * Replace illegal directory characters in a show's name with underscores.
     *
     * @param showName Name of podcast.
     * @return The given podcast name with illegal folder characters replaced.
     */
    public static String getOutputFolderFromShowName(String showName) {
        return showName.replaceAll(ILLEGAL_FOLDER_NAME_REGEX, UNDERSCORE).toLowerCase();
    }

    /**
     * Replace illegal file name characters with underscores.
     *
     * @param str File name to remove characters from.
     * @return The given file name with illegal characters replaced.
     */
    public static String stripIllegalOSCharacters(String str) {
        return str.replaceAll(ILLEGAL_OS_CHARACTER_REGEX, UNDERSCORE);
    }

    /**
     * Parse the given date string and convert it into a desired format.
     *
     * @param dateStr String representation of a date from the RSS feed.
     * @return Date string to show for an episode.
     */
    public static String parseEpisodeDate(String dateStr) {
        try {
            final DateFormat df1 = new SimpleDateFormat(EPISODE_DATE_PATTERN);
            final Date parsedDate = df1.parse(dateStr);
            final DateFormat df = new SimpleDateFormat(EPISODE_DATE_OUTPUT);
            return df.format(parsedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateStr;
    }

    /**
     * Parse the duration of a podcast episode.
     *
     * @param durationInSeconds String representation of seconds.
     * @return A formatted duration string for display to a user.
     */
    public static String parseEpisodeLength(String durationInSeconds) {
        try {
            if (Pattern.matches(EPISODE_DURATION_REGEX, durationInSeconds)) {
                return durationInSeconds;
            }

            int parsedSeconds = Integer.parseInt(durationInSeconds);

            int seconds = (parsedSeconds) % 60 ;
            int minutes = ((parsedSeconds / 60) % 60);
            int hours   = ((parsedSeconds / (60*60)) % 24);

            return String.format("%02d:%02d:%02d", hours, minutes, seconds);
        } catch (NumberFormatException e) {
            LOG.error("Failed to parse episode length '{}'", durationInSeconds);
        }

        return durationInSeconds;
    }

    /**
     * Generate a compressed version of the image in the RSS feed to be attached to downloaded episodes.
     *
     * @param outputFile A {@link File} representing the output path of the generated image.
     * @param episodeDTO DTO with episode information.
     * @return True if the image was created and saved; false otherwise.
     */
    public static boolean compressAndSavePodcastImage(File outputFile, EpisodeDTO episodeDTO) {
        try {
            final URL imageURL = new URL(episodeDTO.getImageUrl());
            final InputStream is = imageURL.openStream();
            final BufferedImage image = ImageIO.read(is);
            is.close();

            // Resize the image
            final Image resizedImage = image.getScaledInstance(MP3_IMAGE_SIZE, MP3_IMAGE_SIZE, Image.SCALE_SMOOTH);

            final BufferedImage convertedBufferedImage =
                    new BufferedImage(MP3_IMAGE_SIZE, MP3_IMAGE_SIZE, BufferedImage.TYPE_INT_RGB);
            final Graphics2D g = convertedBufferedImage.createGraphics();
            g.drawImage(resizedImage, 0, 0, Color.WHITE, null);
            g.dispose();

            final FileOutputStream fos = new FileOutputStream(outputFile);
            final boolean wroteImage = ImageIO.write(convertedBufferedImage, "png", fos);
            fos.close();

            if (!wroteImage) {
                throw new IllegalStateException("Failed to write image!");
            }
        } catch (IOException | IllegalStateException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * Get the final output file name of the saved podcast episode.
     *
     * Ex: "PodcastName - E00001.mp3"
     *
     * @param podcastName Name of the podcast.
     * @param episodeNumber The episode number.
     * @return Final name of the podcast episode.
     */
    public static String getFormattedEpisodeFilename(String podcastName, int episodeNumber) {
        final String paddedEpisodeNo = "E" + String.format("%05d", episodeNumber);
        return String.format(
                "%s - %s.mp3",
                RSSUtils.stripIllegalOSCharacters(podcastName),
                paddedEpisodeNo
        );
    }

    /**
     * Given a directory path, create the directory in the filesystem, if missing.
     *
     * @param dirPath Path to confirm exists in the filesystem.
     */
    public static void makeDirIfMissing(String dirPath) {
        final File dir = new File(dirPath);
        if (!dir.exists() && dir.mkdirs()) {
            LOG.debug("Made directory: {}", dirPath);
        }
    }
}
