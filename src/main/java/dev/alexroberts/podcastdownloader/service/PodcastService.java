package dev.alexroberts.podcastdownloader.service;

import com.mpatric.mp3agic.ID3v2;
import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.NotSupportedException;
import com.mpatric.mp3agic.UnsupportedTagException;
import dev.alexroberts.podcastdownloader.model.EpisodeDTO;
import dev.alexroberts.podcastdownloader.model.xml.RSSChannel;
import dev.alexroberts.podcastdownloader.repository.PoorMansDatabaseService;
import dev.alexroberts.podcastdownloader.repository.model.Podcast;
import dev.alexroberts.podcastdownloader.service.xml.PodcastRssHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import static dev.alexroberts.podcastdownloader.Constants.IMAGE_DIR;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Optional;
import java.util.UUID;

/**
 * Service for interacting with podcast feeds and podcast episodes.
 */
@Service
public class PodcastService {
    private static final Logger LOG = LoggerFactory.getLogger(PodcastService.class);

    private static final String PNG_MIMETYPE = "image/png";

    @Autowired
    private FilePathBuilder filePathBuilder;

    @Autowired
    private PoorMansDatabaseService databaseService;

    /**
     * Open the passed RSS URL and attempt to parse it into a POJO.
     *
     * <a href="https://www.digitalocean.com/community/tutorials/java-sax-parser-example">SAX Parser Example</a>
     * @param rssUrl URL of the Podcast RSS feed.
     * @return Optional of the parsed RSS Channel object.
     */
    public Optional<RSSChannel> readRssFeed(String rssUrl) {
        final SAXParserFactory parserFactory = SAXParserFactory.newInstance();
        try (InputStream is = new URL(rssUrl).openStream()) {
            SAXParser saxParser = parserFactory.newSAXParser();
            PodcastRssHandler handler = new PodcastRssHandler();
            saxParser.parse(is, handler);

            final RSSChannel RSSChannel = handler.finalizeChannel(filePathBuilder);
            return Optional.of(RSSChannel);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            LOG.error("Error reading RSS feed.", e);
        }

        return Optional.empty();
    }

    public boolean addPodcast(String rssUrl) {
        Optional<RSSChannel> channelOpt = readRssFeed(rssUrl);
        if (channelOpt.isEmpty()) {
            LOG.error("Failed to add new podcast: no RSS feed found.");
            return false;
        }

        final RSSChannel channel = channelOpt.get();
        final UUID newPodcastId = UUID.randomUUID();
        downloadPodcastImage(newPodcastId, channel.getImage().getUrl());
        Podcast newPodcast = new Podcast(
            newPodcastId,
            channel.getTitle(),
            channel.getDescription(),
            rssUrl,
            String.format("%s%s", newPodcastId.toString(), ".png")
        );
        databaseService.addPodcast(newPodcast);

        return true;
    }

    /**
     * Download the podcast episode MP3 and set the ID3 tags.
     *
     * @param episodeDTO DTO with episode information.
     * @return True if episode downloaded successfully; false otherwise.
     */
    public boolean downloadPodcastEpisode(EpisodeDTO episodeDTO) {
        final String episodeUrl = episodeDTO.getItem().getUrl();

        // Skip if already downloaded.
        final String finalPath = filePathBuilder.getFinalEpisodePath(episodeDTO);
        if ((new File(finalPath)).exists()) {
            LOG.debug("Episode at '{}' already exists.", finalPath);
            return true;
        }

        LOG.debug("Grabbing episode from URL '{}'", episodeUrl);
        final String tempFilePath = filePathBuilder.getTempEpisodePath(episodeDTO);
        try (
                BufferedInputStream in = new BufferedInputStream(new URL(episodeUrl).openStream());
                FileOutputStream out = new FileOutputStream(tempFilePath);
        ) {
            final byte[] dataBuffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = in.read(dataBuffer, 0, 4096)) != -1) {
                out.write(dataBuffer, 0, bytesRead);
            }

            LOG.debug("Saved file to '{}'.", finalPath);
        } catch (IOException e) {
            LOG.error("Failed to write stream to file.", e);
            return false;
        }

        final String albumArtworkPath = filePathBuilder.getPodcastImagePath(episodeDTO);
        tryToAddID3Tags(episodeDTO, tempFilePath, albumArtworkPath, finalPath);
        deleteTempFile(tempFilePath);
        return true;
    }

    /**
     * Set the MP3 ID3 tags for the downloaded episode.
     *
     * @param episodeDTO DTO with episode information.
     * @param tempFilePath Path to temp location of episode.
     * @param albumArtworkPath Path to MP3 album artwork.
     * @param finalFilePath Path to final output of episode.
     */
    private void tryToAddID3Tags(
            EpisodeDTO episodeDTO,
            String tempFilePath,
            String albumArtworkPath,
            String finalFilePath
    ) {
        try {
            final Mp3File mp3File = new Mp3File(tempFilePath);

            final ID3v2 tagV2 = mp3File.getId3v2Tag();

            final String episodeName = episodeDTO.getItem().getTitle();
            tagV2.setTitle(episodeName);

            final String podcastName = episodeDTO.getPodcastName();
            tagV2.setAlbumArtist(podcastName);
            tagV2.setAlbum(podcastName);

            final String episodeNumber = String.valueOf(episodeDTO.getItem().getEpisodeNumber());
            tagV2.setTrack(episodeNumber);

            // Set the episode description as the ID3 comment.
            tagV2.setComment(episodeDTO.getItem().getDescription());

            final String guid = episodeDTO.getItem().getGuid();
            if (guid != null && guid.trim().length() > 0) {
                tagV2.setKey(guid);
            }

            File albumArtwork = new File(albumArtworkPath);
            if (albumArtwork.exists() || RSSUtils.compressAndSavePodcastImage(albumArtwork, episodeDTO)) {
                tagV2.setAlbumImage(Files.readAllBytes(albumArtwork.toPath()), PNG_MIMETYPE);
            }

            mp3File.save(finalFilePath);
        } catch (IOException | UnsupportedTagException | InvalidDataException | NotSupportedException e) {
            LOG.error("Failed to set ID3 Tags.", e);
        }
    }

    /**
     * Delete the temporary mp3 file.
     *
     * @param tempFilePath Path of the mp3 file.
     */
    private void deleteTempFile(String tempFilePath) {
        final File tempFile = new File(tempFilePath);
        if (tempFile.exists()) {
            if (tempFile.delete()) {
                LOG.debug("Deleted temporary file '{}'", tempFile.getPath());
            }
        }
    }

    private void downloadPodcastImage(UUID podcastId, String imageURL) {
        final String outputLocation = IMAGE_DIR + "/" + podcastId.toString() + ".png";
        try (
            InputStream is = new URL(imageURL).openStream()
        ) {
            Files.copy(is, Paths.get(outputLocation), StandardCopyOption.REPLACE_EXISTING);
            LOG.debug("Saved file to '{}'.", outputLocation);
        } catch (IOException e) {
            LOG.error("Failed to write stream to file.", e);
        }
    }
}
