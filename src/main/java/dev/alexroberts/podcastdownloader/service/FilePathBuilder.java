package dev.alexroberts.podcastdownloader.service;

import dev.alexroberts.podcastdownloader.model.EpisodeDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import static dev.alexroberts.podcastdownloader.Constants.IMAGE_DIR;
import static dev.alexroberts.podcastdownloader.Constants.TEMP_DIR;

/**
 * A service to build out file paths for various uses based on application configs.
 */
@Service
public class FilePathBuilder {
    private static final String FILE_SEPARATOR = "/";

    @Value("${configs.directories.downloads:./_downloads}")
    private String finalDownloadDir;

    /**
     * Get the path the podcast's displayed image will be saved at.
     *
     * @param episodeDTO DTO with episode information.
     * @return Path to podcast image.
     */
    public String getPodcastImagePath(EpisodeDTO episodeDTO) {
        final String folderName = RSSUtils.getOutputFolderFromShowName(episodeDTO.getPodcastName());
        return IMAGE_DIR + FILE_SEPARATOR + folderName + ".png";
    }

    /**
     * Get the path, with file name, of the temporary download location of the podcast episode.
     *
     * @param episodeDTO DTO with episode information.
     * @return Path to temporary podcast episode location.
     */
    public String getTempEpisodePath(EpisodeDTO episodeDTO) {
        final String tempDlDir = removeTrailingSlash(TEMP_DIR);
        RSSUtils.makeDirIfMissing(tempDlDir);
        final String filename = RSSUtils.getFormattedEpisodeFilename(
                episodeDTO.getPodcastName(),
                episodeDTO.getItem().getEpisodeNumber()
        );
        return tempDlDir + FILE_SEPARATOR + filename;
    }

    /**
     * Get the relative path the final podcast episode will be saved to.
     *
     * @param podcastName Name of the podcast; determines folder name.
     * @return Relative path to final output file directory.
     */
    public String getFinalDownloadDir(String podcastName) {
        final String finalDlDir = removeTrailingSlash(finalDownloadDir);
        final String formattedShowName = RSSUtils.getOutputFolderFromShowName(podcastName);
        return finalDlDir + FILE_SEPARATOR + formattedShowName;
    }

    /**
     * Get the relative path, with file name, of the final output file.
     *
     * @param episodeDTO DTO with episode information.
     * @return Relative file path of final output file.
     */
    public String getFinalEpisodePath(EpisodeDTO episodeDTO) {
        final String podcastName = episodeDTO.getPodcastName();
        final String finalDir = getFinalDownloadDir(podcastName);
        RSSUtils.makeDirIfMissing(finalDir);
        final int episodeNo = episodeDTO.getItem().getEpisodeNumber();
        return finalDir + FILE_SEPARATOR + RSSUtils.getFormattedEpisodeFilename(podcastName, episodeNo);
    }

    /**
     * Remove the trailing '/' from the given string, if there.
     *
     * @param str String to strip character from.
     * @return The given string without any trailing '/' character.
     */
    private String removeTrailingSlash(String str) {
        return str.endsWith(FILE_SEPARATOR) ? str.substring(0, str.length() - 1) : str;
    }
}
