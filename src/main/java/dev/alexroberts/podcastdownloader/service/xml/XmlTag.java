package dev.alexroberts.podcastdownloader.service.xml;

/**
 * Enum representing the various RSS tags that will be parsed by {@link PodcastRssHandler}.
 */
public enum XmlTag {
    CHANNEL("channel"),
    CHANNEL_TITLE("title"),
    CHANNEL_LINK("link"),
    CHANNEL_DESCRIPTION("description"),

    IMAGE("image"),
    IMAGE_URL("url"),
    IMAGE_TITLE("title"),
    IMAGE_LINK("link"),

    ITEM("item"),
    ITEM_TITLE("title"),
    ITEM_DESCRIPTION("description"),
    ITEM_DATE("pubDate"),
    ITEM_LENGTH("itunes:duration"),
    ITEM_GUID("guid"),
    ITEM_ENCLOSURE("enclosure")
    ;

    private final String tag;

    XmlTag(String tag) {
        this.tag = tag;
    }

    public boolean matches (String other) {
        return this.tag.equalsIgnoreCase(other);
    }
}
