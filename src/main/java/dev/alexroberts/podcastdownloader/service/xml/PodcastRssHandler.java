package dev.alexroberts.podcastdownloader.service.xml;

import dev.alexroberts.podcastdownloader.model.xml.RSSChannel;
import dev.alexroberts.podcastdownloader.model.xml.Image;
import dev.alexroberts.podcastdownloader.model.xml.RSSItem;
import dev.alexroberts.podcastdownloader.service.FilePathBuilder;
import dev.alexroberts.podcastdownloader.service.RSSUtils;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import static dev.alexroberts.podcastdownloader.service.xml.XmlTag.*;

import java.io.File;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * XML 'handler' for parsing the incoming RSS feed information into a Java object.
 */
public class PodcastRssHandler extends DefaultHandler {

    private StringBuilder data = null;

    private final RSSChannel rssChannel;

    private RSSItem rssItem;
    private int itemOffset = 0;
    private boolean isReadingItem = false;

    private Image image;
    private boolean isReadingImage = false;

    /**
     * Constructor. Creates new instance of internal {@link RSSChannel}.
     */
    public PodcastRssHandler() {
        this.rssChannel = new RSSChannel();
    }

    /**
     * Perform actions on the Channel data once all items have been read from the RSS feed.
     * - Create output dir of podcast if missing
     * - Find any existing files at the output dir, and mark them as already downloaded
     *
     * @param filePathBuilder builder for output file paths.
     * @return A 'finalized' {@link RSSChannel} object.
     */
    public RSSChannel finalizeChannel(FilePathBuilder filePathBuilder) {
        final String outputFolderPath = filePathBuilder.getFinalDownloadDir(this.rssChannel.getTitle());
        RSSUtils.makeDirIfMissing(outputFolderPath);
        final File outputFolder = new File(outputFolderPath);
        Set<String> existingFilenames = Arrays.stream(Objects.requireNonNull(outputFolder.listFiles()))
                .filter(File::isFile)
                .map(File::getName)
                .collect(Collectors.toSet());

        final int totalEpisodes = this.rssChannel.getItems().size();
        this.rssChannel.getItems().forEach(rssItem -> {
            rssItem.setEpisodeNumber(totalEpisodes - rssItem.getEpisodeOffset());
            if (existingFilenames.contains(RSSUtils.getFormattedEpisodeFilename(this.rssChannel.getTitle(), rssItem.getEpisodeNumber()))) {
                rssItem.setAlreadyExistsOnDisk(true);
            }
        });

        return this.rssChannel;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

        if (IMAGE.matches(qName)) {
            image = new Image();
            isReadingImage = true;
        } else if (ITEM.matches(qName)) {
            rssItem = new RSSItem();
            rssItem.setEpisodeOffset(itemOffset++);
            isReadingItem = true;
        }

        if (ITEM_ENCLOSURE.matches(qName) && isReadingItem) {
            rssItem.setUrl(attributes.getValue("url"));
        }

        data = new StringBuilder();
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (isReadingImage) {
            if (IMAGE.matches(qName)) {
                rssChannel.setImage(image);
                isReadingImage = false;
            } else if (IMAGE_URL.matches(qName)) {
                image.setUrl(data.toString());
            } else if (IMAGE_TITLE.matches(qName)) {
                image.setTitle(data.toString());
            } else if (IMAGE_LINK.matches(qName)) {
                image.setLink(data.toString());
            }
        } else if (isReadingItem) {
            if (ITEM.matches(qName)) {
                rssChannel.addItem(rssItem);
                isReadingItem = false;
            } else if (ITEM_TITLE.matches(qName)) {
                rssItem.setTitle(data.toString());
            } else if (ITEM_DATE.matches(qName)) {
                rssItem.setPubDate(RSSUtils.parseEpisodeDate(data.toString()));
            } else if (ITEM_DESCRIPTION.matches(qName)) {
                rssItem.setDescription(RSSUtils.cleanUpDescription(data.toString()));
            } else if (ITEM_LENGTH.matches(qName)) {
                rssItem.setDuration(RSSUtils.parseEpisodeLength(data.toString()));
            } else if (ITEM_GUID.matches(qName)) {
                rssItem.setGuid(data.toString());
            }
        } else {
            if (CHANNEL_TITLE.matches(qName)) {
                rssChannel.setTitle(data.toString());
            } else if (CHANNEL_DESCRIPTION.matches(qName)) {
                rssChannel.setDescription(data.toString());
            } else if (CHANNEL_LINK.matches(qName)) {
                rssChannel.setLink(data.toString());
            }
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        data.append(new String(ch, start, length));
    }
}
