package dev.alexroberts.podcastdownloader.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import dev.alexroberts.podcastdownloader.model.EpisodeDTO;
import dev.alexroberts.podcastdownloader.model.xml.RSSChannel;
import dev.alexroberts.podcastdownloader.repository.PoorMansDatabaseService;
import dev.alexroberts.podcastdownloader.repository.model.Podcast;
import dev.alexroberts.podcastdownloader.service.PodcastService;

@Controller
public class APIController {

    @Autowired
    private PodcastService podcastService;

    @Autowired
    private PoorMansDatabaseService databaseService;

    /**
     * Provided an RSS URL, parse it and return a {@link RSSChannel} object.
     *
     * @param rssUrl URL of a podcast RSS feed.
     * @return Response containing the parsed RSS feed.
     */
    @ResponseBody
    @GetMapping("/api/readRss")
    public ResponseEntity<RSSChannel> readRssFeed(@RequestParam(name = "url") String rssUrl) {
        if (rssUrl == null || rssUrl.trim().length() == 0) {
            return new ResponseEntity<>(new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }

        final Optional<RSSChannel> channelOpt = podcastService.readRssFeed(rssUrl);
        if (channelOpt.isEmpty()) {
            return new ResponseEntity<>(new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(channelOpt.get(), HttpStatus.OK);
    }

    /**
     * Download the provided podcast episode.
     *
     * @param episodeDTO DTO with episode information.
     * @return Response with status of download request.
     */
    @PostMapping("/api/downloadPodcast")
    public ResponseEntity<String> downloadPodcast(@RequestBody EpisodeDTO episodeDTO) {
        if (episodeDTO == null || episodeDTO.getItem() == null) {
            return new ResponseEntity<>("Invalid Request.", HttpStatus.BAD_REQUEST);
        }

        final boolean success = podcastService.downloadPodcastEpisode(episodeDTO);
        if (!success) {
            return new ResponseEntity<>("Error. Check server log.", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>("Okay.", HttpStatus.OK);
    }

    @ResponseBody
    @GetMapping("/api/podcast/list")
    public ResponseEntity<List<Podcast>> getPodcasts() {
        return new ResponseEntity<>(databaseService.listPodcasts(), HttpStatus.OK);
    }

    @PostMapping("/api/addPodcast")
    public ResponseEntity<String> addPodcast(@RequestParam(name = "url") String rssUrl) {
        if (rssUrl == null || rssUrl.trim().length() == 0) {
            return new ResponseEntity<>(new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }

        final boolean success = podcastService.addPodcast(rssUrl);
        if (!success) {
            return new ResponseEntity<>("Error.", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>("Okay.", HttpStatus.OK);
    }
}
