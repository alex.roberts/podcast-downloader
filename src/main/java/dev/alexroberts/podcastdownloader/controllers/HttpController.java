package dev.alexroberts.podcastdownloader.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import dev.alexroberts.podcastdownloader.model.xml.RSSChannel;
import dev.alexroberts.podcastdownloader.repository.PoorMansDatabaseService;
import dev.alexroberts.podcastdownloader.repository.model.Podcast;
import dev.alexroberts.podcastdownloader.service.PodcastService;

/**
 * Rest endpoints for the app.
 */
@Controller
public class HttpController {

    @Autowired
    private PodcastService podcastService;

    @Autowired
    private PoorMansDatabaseService databaseService;

    @GetMapping("/")
    public String homePage(Model model) {
        model.addAttribute("podcasts", databaseService.listPodcasts());
        return "home";
    }

    @GetMapping("/podcasts/{id}")
    public String podcast(@PathVariable("id") String podcastId, Model model) {
        Optional<Podcast> podcastOpt = databaseService.findById(podcastId);
        if (podcastOpt.isEmpty()) {
            return "podcast"; // Should probably do some actual error handling at some point
        }
        final Podcast podcast = podcastOpt.get();
        Optional<RSSChannel> channelOpt = podcastService.readRssFeed(podcast.rssFeedUrl());
        if (channelOpt.isEmpty()) {
            return "podcast";
        }
        model.addAttribute("rssChannel", channelOpt.get());
        return "podcast";
    }
}
