package dev.alexroberts.podcastdownloader.model;

import dev.alexroberts.podcastdownloader.model.xml.RSSItem;

/**
 * Simple DTO sent by the front end when downloading a podcast episode.
 */
public class EpisodeDTO {
    private String podcastName;
    private String imageUrl;
    private RSSItem item;

    public EpisodeDTO() {
        // Do nothing.
    }

    public String getPodcastName() {
        return podcastName;
    }

    public void setPodcastName(String podcastName) {
        this.podcastName = podcastName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImage(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public RSSItem getItem() {
        return item;
    }

    public void setItem(RSSItem item) {
        this.item = item;
    }
}
