package dev.alexroberts.podcastdownloader.model.xml;

/**
 * An image in the RSS Channel element.
 */
public class Image {
    private String url;
    private String title;
    private String link;

    /**
     * Default Constructor.
     */
    public Image() {
        // Do nothing.
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
