package dev.alexroberts.podcastdownloader.model.xml;

/**
 * POJO representation of the 'item' tag of the RSS spec.
 */
public class RSSItem {
    // RSS properties
    private String title;
    private String description;
    private String pubDate;
    private String duration;
    private String guid;
    private String url;

    // Offset from being the most recent episode. Let's me do [total episodes] - [offset] = [episode number].
    private int episodeOffset = 0;
    private int episodeNumber;
    private boolean alreadyExistsOnDisk = false;

    /**
     * Default Constructor.
     */
    public RSSItem() {
        // Do nothing.
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getEpisodeOffset() {
        return episodeOffset;
    }

    public void setEpisodeOffset(int episodeOffset) {
        this.episodeOffset = episodeOffset;
    }

    public int getEpisodeNumber() {
        return episodeNumber;
    }

    public void setEpisodeNumber(int episodeNumber) {
        this.episodeNumber = episodeNumber;
    }

    public boolean isAlreadyExistsOnDisk() {
        return alreadyExistsOnDisk;
    }

    public void setAlreadyExistsOnDisk(boolean alreadyExistsOnDisc) {
        this.alreadyExistsOnDisk = alreadyExistsOnDisc;
    }
}
