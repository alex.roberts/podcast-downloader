package dev.alexroberts.podcastdownloader.model.xml;

import java.util.ArrayList;
import java.util.List;

/**
 * POJO representation of the 'channel' tag in the RSS spec.
 */
public class RSSChannel {
    private String title;
    private String link;
    private String description;
    private Image image;
    private final List<RSSItem> items = new ArrayList<>();

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public List<RSSItem> getItems() {
        return items;
    }

    public void addItem(RSSItem RSSItem) {
        this.items.add(RSSItem);
    }
}
