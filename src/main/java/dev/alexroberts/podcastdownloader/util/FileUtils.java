package dev.alexroberts.podcastdownloader.util;

import java.io.File;

public final class FileUtils {
    
    public static boolean isValidFile(String uri) {
        File f = new File(uri);
        return f.exists() && !f.isDirectory();
    }
}
