package dev.alexroberts.podcastdownloader.repository;

import static dev.alexroberts.podcastdownloader.Constants.DATABASE_DIR;

/**
 * Enum representing our "tables", which are really just
 * the names of the JSON files we're storing stuff in.
 */
public enum DatabaseTable {
    PODCAST("podcast.json");

    private String filePath;

    private DatabaseTable(String fileName) {
        this.filePath = String.format("%s/%s", DATABASE_DIR, fileName);
    }

    public String getFilePath() {
        return filePath;
    }
}
