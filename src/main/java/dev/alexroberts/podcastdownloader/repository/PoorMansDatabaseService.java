package dev.alexroberts.podcastdownloader.repository;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.stereotype.Service;

import dev.alexroberts.podcastdownloader.repository.model.Podcast;
import dev.alexroberts.podcastdownloader.service.json.JSONFileUtil;

/**
 * A "database" service that reads and writes to JSON files to store data.
 */
@Service
public class PoorMansDatabaseService {

    public Optional<Podcast> findById(String podcastId) {
        return listPodcasts().stream()
            .filter(p -> Objects.equals(podcastId, p.id().toString()))
            .findFirst();
    }

    public List<Podcast> listPodcasts() {
        return JSONFileUtil.readJSONArrayFromFile(DatabaseTable.PODCAST.getFilePath(), Podcast.class).orElse(List.of());
    }

    public void addPodcast(Podcast podcast) {
        List<Podcast> podcasts = listPodcasts();
        podcasts.add(podcast);
        JSONFileUtil.writeFile(DatabaseTable.PODCAST.getFilePath(), podcasts);
    }
}
