package dev.alexroberts.podcastdownloader.repository.model;

import java.util.UUID;

public record Podcast(
    UUID id,
    String name,
    String description,
    String rssFeedUrl,
    String imageUrl
) {
    
}
