'use strict';

const CHECK_ICON_CLASS = 'fa-check'
const DOWNLOAD_ICON_CLASS = 'fa-download';
const CURSOR_AUTO_CLASS = 'cursor-auto';
const CURSOR_POINTER_CLASS = 'cursor-pointer';

const toggleDownloadIcon = (downloadBtnId) => {
  const $downloadBtn = $(downloadBtnId);
  $downloadBtn.prop('disabled', true);
  $downloadBtn.removeClass(CURSOR_POINTER_CLASS).addClass(CURSOR_AUTO_CLASS);
  $downloadBtn.find('i').each((cnt,x) => $(x).removeClass(DOWNLOAD_ICON_CLASS).addClass(CHECK_ICON_CLASS));
};

const downloadPodcastEpisode = (episodeData, callback) => {
  const requestObj = {
    podcastName: $('#podcast-title').data('title'),
    imageUrl: $('#podcast-image-url').data('image-url'),
    item: episodeData
  };

  toggleDownloadIcon(`#${episodeData.guid}`);

  fetch('/api/downloadPodcast', {
    method: 'POST',
    headers: {
      'Accept': 'application/json, text/plain, */*',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(requestObj)
  })
    .then(() => !!callback ? callback() : null)
    .catch(err => console.log(err));
};

(function () {
  $('.episode-download-btn').each((cnt, btn) => {
    const $btn = $(btn);
    $btn.on('click', () => {
        let itemData = $btn.data('item');
        downloadPodcastEpisode(itemData);
    });
  });

  $('#downloadAllBtn').on('click', () => {
    let items = [];
    $('.episode-download-btn').each((cnt, btn) => {
        items.push($(btn).data('item'));
    });

    items.filter(item => !item.alreadyExistsOnDisk)
      .reduce( async (prev, curr) => {
        await prev;
        return new Promise((res) => downloadPodcast(curr, res));
      }, Promise.resolve());
  });
})();
