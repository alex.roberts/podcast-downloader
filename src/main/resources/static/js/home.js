'use strict';

const readRSSBtn = () => {
  const rssUrlElement = document.getElementById('rssUrlInput');
  const rssUrl = rssUrlElement.value;

  const searchParams = new URLSearchParams({url: rssUrl});
  fetch(`/api/addPodcast?` + searchParams, {method: 'POST'})
    .then(response => {
      const status = response.status;
      if (status === 200) {
          location.reload();
      } else if (status === 500) {
        alert('Error reading RSS');
      } else if (status === 400) {
        alert('Invalid Request!');
      }
    })
    .catch((err) => {console.log(err);});

  rssUrlElement.value = '';
};

(function () {
  document.getElementById('readRssBtn').addEventListener('click', readRSSBtn);
})();
