package dev.alexroberts.podcastdownloader.service;

import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static dev.alexroberts.podcastdownloader.service.RSSUtils.EPISODE_DATE_OUTPUT;
import static dev.alexroberts.podcastdownloader.service.RSSUtils.EPISODE_DATE_PATTERN;
import static org.junit.jupiter.api.Assertions.*;

class RSSUtilsTest {

    private static final String DATE_STRING_1 = "Thu, 19 May 2022 21:10:58 -0000";

    @Test
    void testParseEpisodeDate() throws ParseException {
        final DateFormat df1 = new SimpleDateFormat(EPISODE_DATE_PATTERN);
        Date parsedDate = df1.parse(DATE_STRING_1);

        final DateFormat df = new SimpleDateFormat(EPISODE_DATE_OUTPUT);
        String output = df.format(parsedDate);

        assertEquals("May 19, 2022", output);
    }
}