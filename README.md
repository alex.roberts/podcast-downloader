# Podcast Downloader

### Description:

Simple podcast downloader I threw together over a weekend. Supply it an RSS feed URL and you can download the entire
podcast, or specific episodes. It will also set some ID3 tags on the downloaded mp3 file. See below for more details.

![Screenshot](ScreenshotOfApp.png)

### Building:

Checkout the repo and build it with `mvn clean install`. Then just run the built JAR with `java -jar app.jar`.

Launches at http://localhost:8080 by default.

This, and output directories, can be changed by supplying an `application.yaml`.
See the existing one for an example.

Built on Maven 3.6.3, Java 11.

This project uses [mp3agic]("https://github.com/mpatric/mp3agic/") to read and write ID3 tags to the outputted mp3s.

### Features:

- Parses the given RSS feed into a readable display
- Provides button to download all available episodes
- Provides button to download one episode at a time
- Will not download a duplicate episode if one is already found at the configured output
- Downloads episodes to a configurable output directory
  - Output path is "{output_directory}/podcast_name/'Podcast Name - E00000.mp3'"
- Downloaded files have ID3 tags set as follows:
  - File name is podcast episode name
  - File comment is podcast episode description
  - Both 'Album artist' and 'Album' are set to the podcast name
  - Number ('#') is set to the episode number
  - Album artwork is set to a compressed version of the image provided in the RSS feed

### TODO:

- mp3agic needs an update in order to set the genre of downloaded files to "Podcast".
- I didn't exhaustively test podcast RSS feeds, so there's bound to be one that breaks things.
- If I really get into podcasts in the future, maybe add the ability to auto download episodes with tracking.


### Service file:
Replace "YOUR_USER" with the user that will run it:

```
[Unit]
Description=Podcast Downloader Service

[Service]
WorkingDirectory=/opt/podcast_downloader
ExecStart=/usr/bin/java -Xms128m -Xmx512m -jar app.jar
User=YOUR_USER
Type=simple
Restart=on-failure
RestartSec=10

[Install]
WantedBy=multi-user.target
```
